---
swagger: "2.0"
info:
  version: "Version: 2.1.0"
  title: "KYPO Adaptive Smart Assistant - API Reference"
host: "localhost:8086"
basePath: "/kypo-adaptive-smart-assistant/api/v1"
tags:
- name: "Adaptive Phases"
schemes:
- "http"
- "https"
paths:
  /adaptive-phases:
    post:
      tags:
      - "Adaptive Phases"
      summary: "Find a suitable task"
      description: "Find a suitable task for a participant trying to get the next\
        \ phase"
      operationId: "findSuitableTaskInPhase"
      parameters:
      - in: "body"
        name: "body"
        description: "smartAssistantInput"
        required: true
        schema:
          $ref: "#/definitions/AdaptiveSmartAssistantInput"
      - name: "accessToken"
        in: "query"
        description: "Training instance access token"
        required: false
        type: "string"
      - name: "userId"
        in: "query"
        description: "User ID"
        required: false
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "Suitable task found."
          schema:
            $ref: "#/definitions/SuitableTaskResponseDto"
        500:
          description: "Unexpected application error"
      security:
      - bearerAuth: []
  /adaptive-phases/instances:
    post:
      tags:
      - "Adaptive Phases"
      summary: "Find a suitable task"
      description: "Find a suitable tasks for participants based on their performance"
      operationId: "findSuitableTaskInPhase"
      parameters:
      - in: "body"
        name: "body"
        description: "smartAssistantInput"
        required: true
        schema:
          type: "array"
          items:
            $ref: "#/definitions/OverallInstancePerformance"
      responses:
        200:
          description: "Suitable task found."
          schema:
            $ref: "#/definitions/SuitableTaskResponseDto"
        500:
          description: "Unexpected application error"
      security:
      - bearerAuth: []
definitions:
  AdaptiveSmartAssistantInput:
    type: "object"
    required:
    - "phase_x"
    - "phase_xtasks"
    - "training_run_id"
    properties:
      training_run_id:
        type: "integer"
        format: "int64"
        example: 1
        description: "The identifier of a given training run representing a given\
          \ participant"
      phase_x:
        type: "integer"
        format: "int64"
        example: 5
        description: "The id of a phase X."
      phase_xtasks:
        type: "integer"
        format: "int32"
        example: 3
        description: "The number of tasks in a phase X."
        minimum: 1
      phase_ids:
        type: "array"
        example: "[1,2,3,4,5]"
        description: "The list of phaseIds (the given phase including the given phases)."
        items:
          type: "integer"
          format: "int64"
      decision_matrix:
        type: "array"
        description: "The decision matrix with weights to compute the students' performance."
        items:
          $ref: "#/definitions/DecisionMatrixRowDTO"
  DecisionMatrixRowDTO:
    type: "object"
    required:
    - "completed_in_time"
    - "id"
    - "keyword_used"
    - "order"
    - "questionnaire_answered"
    - "related_phase_info"
    - "solution_displayed"
    - "wrong_answers"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: 1
        description: "ID of decision matrix row"
      order:
        type: "integer"
        format: "int32"
        example: 1
        description: "Order of row in a decision matrix"
      questionnaire_answered:
        type: "number"
        format: "double"
        example: 0.5
        description: "It determines how important the answers of the questions in\
          \ questionnaires are"
      keyword_used:
        type: "number"
        format: "double"
        example: 0.5
        description: "It determines how important it is whether the player used the\
          \ keyword"
      completed_in_time:
        type: "number"
        format: "double"
        example: 0.5
        description: "It determines how important it is whether the player completed\
          \ the task in time"
      solution_displayed:
        type: "number"
        format: "double"
        example: 0.5
        description: "It determines how important it is whether the player displayed\
          \ the solution of the task they were solving"
      wrong_answers:
        type: "number"
        format: "double"
        example: 0.5
        description: "It determines how important the number of wrong answers are"
      related_phase_info:
        example: "1"
        description: "Info about phase (usually training phase) the decision matrix\
          \ row is related to"
        $ref: "#/definitions/DecisionMatrixRowDTO"
      allowed_commands:
        type: "integer"
        format: "int64"
        example: 10
        description: "Number of commands that are allowed to use in a training phase"
      allowed_wrong_answers:
        type: "integer"
        format: "int64"
        example: 10
        description: "Number of wrong answers that are allowed in a training phase"
  OverallInstancePerformance:
    type: "object"
    properties:
      trainee_id:
        type: "integer"
        format: "int64"
        example: 1
        description: "The identifier of a given training run representing a given\
          \ participant"
      smart_assistant_input:
        type: "array"
        description: "List of input values for adaptive smart assistant for each phase\
          \ together with decision matrix"
        items:
          $ref: "#/definitions/AdaptiveSmartAssistantInput"
      phases_smart_assistant_input:
        type: "object"
        description: "Performance statistics for each phase"
        additionalProperties:
          $ref: "#/definitions/OverallPhaseStatistics"
  OverallPhaseStatistics:
    type: "object"
    properties:
      phase_id:
        type: "integer"
        format: "int64"
        example: 1
        description: "ID of a phase"
      task_id:
        type: "integer"
        format: "int64"
        example: 3
        description: "ID of a task"
      sandbox_id:
        type: "integer"
        format: "int64"
        example: 1
        description: "ID of a sandbox"
      phase_order:
        type: "integer"
        format: "int64"
        example: 0
        description: "Order of a phase"
      phase_time:
        type: "integer"
        format: "int64"
        example: 1614803536837
        description: "ID of a task"
      wrong_answers:
        type: "array"
        example: "[\"nmap 123\", \"nmap 123\"]"
        description: "The list of answers (flags) that participant submitted"
        items:
          type: "string"
      solution_displayed:
        type: "boolean"
        example: true
        description: "The information if the solution was displayed"
      number_of_commands:
        type: "integer"
        format: "int64"
        example: 5
        description: "The number of submitted commands"
      keywords_in_commands:
        type: "object"
        description: "The map containing the mapping if the commands contains the\
          \ right keywords"
        additionalProperties:
          type: "integer"
          format: "int64"
      questions_answer:
        type: "array"
        example: "[true,false,true,true]"
        description: "List of trainees answers"
        items:
          type: "boolean"
  SuitableTaskResponseDto:
    type: "object"
    properties:
      suitable_task:
        type: "integer"
        format: "int32"
        example: 1
        description: "Returns the number representing the suitable task for a given\
          \ participant"
